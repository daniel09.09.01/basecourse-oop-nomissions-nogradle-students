package AIF.Modules;

import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;

public abstract class Module {
    private int actionRange;

    int getActionRange(){
        return this.actionRange;
    }

    void setActionRange(int range){
        this.actionRange = range;
    }

    public abstract void activate() throws NoModuleCanPerformException;

    public abstract String getName();
}

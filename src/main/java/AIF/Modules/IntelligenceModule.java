package AIF.Modules;

import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;

public class IntelligenceModule extends Module {

    public IntelligenceModule(){
        this.setActionRange(600);
    }

    @Override
    public void activate() throws NoModuleCanPerformException {
        System.out.println("Intelligence module is activating");
    }

    @Override
    public String getName() {
        return "Intelligence";
    }
}

package AIF.Modules;

import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;

public class AttackModule extends Module {
    private boolean launched;

    public AttackModule() {
        this.setActionRange(1500);
        this.setLaunched(false);
    }

    public boolean isLaunched() {
        return this.launched;
    }

    public void setLaunched(boolean launched) {
        this.launched = launched;
    }

    @Override
    public void activate() throws NoModuleCanPerformException {
        if (this.isLaunched()) {
            throw new NoModuleCanPerformException();
        } else {
            System.out.println("Attack module is activating.");
            this.setLaunched(true);
        }

    }

    @Override
    public String getName() {
        return "Attack";
    }
}

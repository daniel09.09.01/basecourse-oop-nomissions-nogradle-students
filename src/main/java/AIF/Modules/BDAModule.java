package AIF.Modules;

import AIF.AerialVehicles.Exceptions.NoModuleCanPerformException;

public class BDAModule extends Module {

    public BDAModule(){
        this.setActionRange(300);
    }

    @Override
    public void activate() throws NoModuleCanPerformException {
        System.out.println("BDA module is activating");
    }

    @Override
    public String getName() {
        return "BDA";
    }
}

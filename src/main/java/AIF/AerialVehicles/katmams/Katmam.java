package AIF.AerialVehicles.katmams;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Entities.Coordinates;

public abstract class Katmam extends AerialVehicle {

    public Katmam(Coordinates coordinates) {
        super(coordinates);
    }

    public void hoverOverLocation(int hours) throws CannotPerformOnGroundException {
        if (this.isFlying()) {
            System.out.println("Hovering over " + this.getCurrentLocation().toString());
            this.setPassedKm(this.getPassedKm() + hours * 150);
        } else {
            System.out.println("Cannot hover over location when katmam is on the ground.");
            throw new CannotPerformOnGroundException();
        }
    }
}

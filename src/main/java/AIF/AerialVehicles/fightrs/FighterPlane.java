package AIF.AerialVehicles.fightrs;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class FighterPlane extends AerialVehicle {

    public FighterPlane(Coordinates coordinates){
        super(coordinates);
    }
}

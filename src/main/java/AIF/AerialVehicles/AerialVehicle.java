package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;
import AIF.Modules.Module;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import AIF.Entities.Consts;

public abstract class AerialVehicle {

    private List<Module> modules;
    private int stations;
    private int maintenance;
    private double lastFixingDistance;
    private boolean isFlying;
    private double passedKm;
    private Coordinates currentLocation;
    private List<Class> possibleModules;

    public AerialVehicle(Coordinates coordinates) {
        this.setFlying(false);
        this.setPassedKm(0);
        this.setStations(Consts.STATIONS.get(this.getClass()));
        this.setMaintenance(Consts.MAINTENANCES.get(this.getClass().getSuperclass()));
        this.setLastFixingDistance(0);
        this.setCurrentLocation(coordinates);
        this.setPossibleModules(Consts.POSSIBLE_MODULES.get(this.getClass()));
        this.setModules(new ArrayList<>());
    }

    public void takeOff() throws CannotPerformInMidAirException {
        if (this.isFlying()) {
            System.out.println("Cannot take off because is not on the land.");
            throw new CannotPerformInMidAirException();
        } else {
            System.out.println("Taking off");
            this.setFlying(true);
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformOnGroundException {
        if (this.isFlying()) {
            System.out.println("Flying to: " + destination.toString());
            this.setPassedKm(this.getPassedKm() + this.currentLocation.distance(destination));
            this.setCurrentLocation(destination);
        } else {
            System.out.println("Cannot fly to destination when vehicle on the ground");
            throw new CannotPerformOnGroundException();
        }
    }

    public void land() throws CannotPerformOnGroundException {
        if (this.isFlying()) {
            System.out.println("Landing");
            this.setFlying(false);
        } else {
            System.out.println("Cannot land when vehicle on the ground");
            throw new CannotPerformOnGroundException();
        }
    }

    public boolean needMaintenance() {
        if (this.getPassedKm() - this.getLastFixingDistance() >= this.getMaintenance()) {
            return true;
        }

        return false;
    }

    public void performMaintenance() throws CannotPerformOnGroundException {
        if (!this.isFlying()) {
            System.out.println("Performing maintenance");
            this.setLastFixingDistance(this.getPassedKm());
        } else {
            System.out.println("Cannot perform maintenance when vehicle is flying");
            throw new CannotPerformOnGroundException();
        }
    }

    public void loadModule(Module moduleToLoad) throws NoModuleCanPerformException, ModuleNotCompatibleException {
        if (this.getModules().size() == this.getStations()) {
            System.out.println("Cannot load module, there is no place");
            throw new NoModuleCanPerformException();
        } else if (this.getPossibleModules().indexOf(moduleToLoad.getClass()) == -1) {
            System.out.println("The module is not legal for this vehicle.");
            throw new ModuleNotCompatibleException();
        } else {
            this.getModules().add(moduleToLoad);
            System.out.println("Module " + moduleToLoad.getName() + " successfully loaded.");
        }
    }

    public void activateModule(Class module, Coordinates coordinates) throws NoModuleStationAvailableException {
        AtomicBoolean isActivated = new AtomicBoolean(false);

        this.getModules().forEach(kit -> {
            if (kit.getClass().equals(module)) {
                try {
                    kit.activate();
                    System.out.println("Module activated successfully on " + coordinates.toString());
                    isActivated.set(true);
                    return;
                } catch (NoModuleCanPerformException e) {
                }
            }
        });

        if(!isActivated.get()){
            System.out.println("There is no available module to activate.");
            throw new NoModuleStationAvailableException();
        }
    }

    public List<Class> getPossibleModules() {
        return this.possibleModules;
    }

    public void setPossibleModules(List<Class> possibleModules) {
        this.possibleModules = possibleModules;
    }

    public boolean isFlying() {
        return this.isFlying;
    }

    public void setFlying(boolean flying) {
        this.isFlying = flying;
    }

    public List<Module> getModules() {
        return this.modules;
    }

    public void setModules(List<Module> modules) {
        this.modules = modules;
    }

    public int getStations() {
        return this.stations;
    }

    public void setStations(int stations) {
        this.stations = stations;
    }

    public int getMaintenance() {
        return this.maintenance;
    }

    public void setMaintenance(int maintenance) {
        this.maintenance = maintenance;
    }

    public double getPassedKm() {
        return this.passedKm;
    }

    public void setPassedKm(double passedKm) {
        this.passedKm = passedKm;
    }

    public Coordinates getCurrentLocation() {
        return this.currentLocation;
    }

    public void setCurrentLocation(Coordinates currentLocation) {
        this.currentLocation = currentLocation;
    }

    public double getLastFixingDistance() {
        return this.lastFixingDistance;
    }

    public void setLastFixingDistance(double lastFixingDistance) {
        this.lastFixingDistance = lastFixingDistance;
    }
}

package AIF.Entities;

import AIF.AerialVehicles.fightrs.F15;
import AIF.AerialVehicles.fightrs.F16;
import AIF.AerialVehicles.fightrs.FighterPlane;
import AIF.AerialVehicles.katmams.*;
import AIF.Modules.AttackModule;
import AIF.Modules.BDAModule;
import AIF.Modules.IntelligenceModule;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class Consts {

    public static final HashMap<Class, Integer> STATIONS = new LinkedHashMap<Class, Integer>(){{
        put(F15.class, 10);
        put(F16.class, 7);
        put(Eitan.class, 4);
        put(Kochav.class, 5);
        put(Shoval.class, 3);
        put(Zik.class, 5);
    }};

    public static final HashMap<Class, Integer> MAINTENANCES = new LinkedHashMap<Class, Integer>(){{
        put(FighterPlane.class, 25000);
        put(Hermes.class, 10000);
        put(Haron.class, 15000);
    }};

    public static final LinkedHashMap<Class, List<Class>> POSSIBLE_MODULES = new LinkedHashMap<Class, List<Class>>(){{
        put(F15.class, new ArrayList<Class>(){{
            add(AttackModule.class);
            add(IntelligenceModule.class);
        }});

        put(F16.class, new ArrayList<Class>(){{
            add(AttackModule.class);
            add(BDAModule.class);
        }});

        put(Eitan.class, new ArrayList<Class>(){{
            add(AttackModule.class);
            add(IntelligenceModule.class);
        }});

        put(Kochav.class, new ArrayList<Class>(){{
            add(AttackModule.class);
            add(BDAModule.class);
            add(IntelligenceModule.class);
        }});

        put(Shoval.class, new ArrayList<Class>(){{
            add(AttackModule.class);
            add(BDAModule.class);
            add(IntelligenceModule.class);
        }});

        put(Zik.class, new ArrayList<Class>(){{
            add(BDAModule.class);
            add(IntelligenceModule.class);
        }});

    }};
}
